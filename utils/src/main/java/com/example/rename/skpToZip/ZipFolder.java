package com.example.rename.skpToZip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFolder {

	public static void main(String[] args) {
		// 指定包含.skp文件的文件夹路径
		String sourceFolder1 = "E:\\BaiduNetdiskDownload\\奥特曼\\348游泳池";
		String sourceFolder2 = "E:\\BaiduNetdiskDownload\\奥特曼\\299新中式茶室会所";
		String sourceFolder3 = "E:\\BaiduNetdiskDownload\\奥特曼\\574中式茶叶店";
		String[] sourceFolders = new String[]{sourceFolder1, sourceFolder2, sourceFolder3};

		// 执行压缩
		for (String sourceFolder : sourceFolders) {
			if (sourceFolder.isEmpty()){
				continue;
			}
			zipSKPFiles(sourceFolder);
		}
	}

	public static void zipSKPFiles(String sourceFolder) {
		try {
			// 获取文件夹中的所有.skp文件
			File folder = new File(sourceFolder);
			File[] skpFiles = folder.listFiles((dir, name) -> name.toLowerCase().endsWith(".skp"));

			for (File skpFile : skpFiles) {
				// 构建输出的zip文件路径（去掉.skp的后缀）
				String zipFilePath = skpFile.getAbsolutePath().substring(0, skpFile.getAbsolutePath().lastIndexOf('.')) + ".zip";

				// 执行压缩
				zipFile(skpFile, zipFilePath);

				System.out.println("File " + skpFile.getName() + " has been compressed successfully!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void zipFile(File inputFile, String zipFilePath) throws IOException {
		try (FileOutputStream fos = new FileOutputStream(zipFilePath);
		     ZipOutputStream zos = new ZipOutputStream(fos);
		     FileInputStream fis = new FileInputStream(inputFile)) {

			ZipEntry zipEntry = new ZipEntry(inputFile.getName());
			zos.putNextEntry(zipEntry);

			byte[] buffer = new byte[1024];
			int len;
			while ((len = fis.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}
		}
	}
}
