package com.example.rename.vr1108;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vr1108Application {

    public static void main(String[] args) {
        SpringApplication.run(Vr1108Application.class, args);
    }

}
