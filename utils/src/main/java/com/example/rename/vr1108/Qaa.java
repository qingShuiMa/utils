package com.example.rename.vr1108;

import lombok.Data;

/**
 * @ClassName: Qaa
 * @Author: zhoute
 * @Date: 2023-11-08
 **/
@Data
public class Qaa {

    private String url;

    private String fileName;

}
