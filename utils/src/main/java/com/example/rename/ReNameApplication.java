package com.example.rename;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReNameApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReNameApplication.class, args);
	}

}
